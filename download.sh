# This script will download youtube channels to your hard drive
#
# Requirements:
# * Homebrew
#   /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
# * Python
#   brew install python
# * youtube-dl
#   brew install youtube-dl
# * ffmpeg
#   brew install ffmpeg
#
# Setup
# * Create a folder
#   cd ~; mkdir -p Videos/youtube; cd Videos/youtube;
# * Create an input file: channels.txt This is simply a list of channels to download. One url per line
#   echo https://www.youtube.com/user/NurdRage > channels.txt
# * Configure this script to be executable
#   chmod +x download.sh
#
# Run the script
# * ./download.sh

youtube-dl \
--format "bestvideo+bestaudio" \
--ignore-errors \
--output "%(uploader)s (%(uploader_id)s)/%(upload_date)s - %(title)s - (%(duration)ss) [%(resolution)s] [%(id)s].%(ext)s" \
--download-archive ./archive.txt \
--batch-file ./channels.txt \
--prefer-ffmpeg \
--write-sub \
--all-subs \
--convert-subs srt \
--add-metadata \
--write-description \
--write-annotations \
--write-thumbnail \
--verbose