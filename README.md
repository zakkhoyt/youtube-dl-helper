
This script will download youtube channels to your hard drive

## Requirements:
Each of the following are required in order to run this tool. These commands will get you running on macOS. 

* Homebrew
````
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
````
* Python
````
brew install python
````
* youtube-dl
````
brew install youtube-dl
````
* ffmpeg
````
brew install ffmpeg
````

## Setup
* Create a folder
````
  cd ~; mkdir -p Videos/youtube; cd Videos/youtube;
````
* Modify the input file (channels.txt). This is simply a list of channels to download. One url per line
````
  echo https://www.youtube.com/user/Codyslab > channels.txt
````
* Configure this script to be executable
````
  chmod +x download.sh
````

## Run the script
````
./download.sh
````